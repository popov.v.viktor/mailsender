let nodemailer = require('nodemailer')
let fs = require('fs')
let path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '.env') })

let header = fs.readFileSync(path.resolve(__dirname, './templates/components/header.html'), 'utf8')
let body = fs.readFileSync(path.resolve(__dirname, './templates/' + process.argv[2]), 'utf8')
let footer = fs.readFileSync(path.resolve(__dirname, './templates/components/footer.html'), 'utf8')

let transport = nodemailer.createTransport({
  host: 'smtp.mailtrap.io',
  port: 2525,
  auth: {
    user: process.env.USER_MAILTRAP,
    pass: process.env.PASSWORD_MAILTRAP
  }
})

let message = {
  from: 'test@test.net',
  to: 'test@test.net',
  subject: 'Message title',
  text: 'Plaintext version of the message',
  html: header + body + footer
}

transport.sendMail(message)
