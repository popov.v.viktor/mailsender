const path = require('path')
const express = require('express')
const app = express()

require('dotenv').config({ path: path.resolve(__dirname, '..', '.env') })

global.app = app

app.use(express.static(__dirname + '/public'))
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, '/views'))

require('fs').readdirSync(path.join(__dirname, 'routes')).forEach(function(file) {
  require('./routes/' + file);
})

app.listen(3000)