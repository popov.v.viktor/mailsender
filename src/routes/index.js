const mailRender = require('../services/mailRender')
const mailSender = require('../services/mailSender')
const currentTemplate = 'iqvector/sample/content'

app.get('/', async function (req, res) {
  const previewEmail = await mailRender.previewEmail(currentTemplate, null, {
    from: process.env.USER_MAIL_FROM,
    to: process.env.USER_MAIL_TO,
    title: 'Title mail',
    subject: 'Subject mail'
  })

  res.set('Content-Type', 'text/html')
  res.send(Buffer.from(previewEmail))
})

app.get('/html', async function (req, res) {
  const htmlEmail = await mailRender.render(currentTemplate, null)

  res.set('Content-Type', 'text/plain')
  res.send(Buffer.from(htmlEmail))
})

app.get('/mail', async function (req, res) {
  await mailSender.send(currentTemplate, null, {
    from: process.env.USER_MAIL_FROM,
    to: process.env.USER_MAIL_TO,
    title: 'Title mail',
    subject: 'Subject mail'
  })

  res.send({ mailSend: true })
})
