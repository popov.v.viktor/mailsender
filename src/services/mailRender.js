const pug = require('pug')
const fs = require('fs/promises')
const path = require('path')
const _ = require('lodash')
const previewEmail = require('preview-email')
const Email = require('email-templates')


class MailRender {
  async render(pathTemplate, locals) {
    const email = new Email({
      views: {
        root: path.resolve(__dirname, '../mails')
      }
    })

    return await email.render({
      path: pathTemplate,
    }, locals || {}).catch(console.error)
  }

  async previewEmail(pathTemplate, locals, message) {
    if (!_.endsWith(pathTemplate, '.pug')) pathTemplate += '.pug'

    return await previewEmail({
      from: 'test@test.net',
      to: 'test@test.net',
      subject: 'Message title',
      text: 'Plaintext version of the message',
      ...(message || {}),
      html: await this.render(pathTemplate, locals = {})
    }, {
      id: 'test',
      dir: path.resolve(__dirname, '../public'),
      open: false
    }).then((file) => {
      return fs.readFile(file.replace('file://', ''), 'utf8')
    })
  }
}


module.exports = new MailRender()