const mailRender = require('./mailRender')
const nodemailer = require('nodemailer')

class MailSender {
  constructor() {
    this.transport = nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: process.env.USER_MAILTRAP,
        pass: process.env.PASSWORD_MAILTRAP
      }
    })
  }

  async send(pathTemplate, locals, message) {
    await this.transport.sendMail({
      ...message,
      html: await mailRender.render(pathTemplate, locals)
    }).catch(console.error)
  }
}

module.exports = new MailSender()